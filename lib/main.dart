import 'package:flutter/material.dart';
import 'package:scanner/presenter/views/mobile_scanner_view.dart';
import 'package:scanner/presenter/views/qr_code_scanner_view.dart';
import 'package:scanner/presenter/views/zxing2_scanner_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Scanner'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _qrCodeScanner(),
            const SizedBox(height: 10,),
            _mobileScanner(),
            const SizedBox(height: 10,),
            _zxing2Scanner(),
            const SizedBox(height: 30),
          ],
        ),
      )
    );
  }

  Widget _qrCodeScanner()=> _buttonAction(callback: (){
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=> const QrCodeScannerView()));
  }, title: 'qrCodeScanner');

  Widget _mobileScanner()=> _buttonAction(callback: (){
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=> const MobileScannerView()));
  }, title: 'mobileScanner');

  Widget _zxing2Scanner()=> _buttonAction(callback: (){
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=> const Zxing2Scanner()));
  }, title: 'zxing2Scanner');

  Widget _buttonAction({VoidCallback? callback, String title = ''})=> ElevatedButton(
    style: ElevatedButton.styleFrom(
        backgroundColor: Colors.blue
    ),
    onPressed: callback ?? (){},
    child: Text(title, style: const TextStyle(color: Colors.white, fontSize: 32),),
  );
}