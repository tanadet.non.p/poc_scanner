import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class MobileScannerView extends StatefulWidget {
  const MobileScannerView({super.key});

  @override
  State<MobileScannerView> createState() => _MobileScannerViewState();
}

class _MobileScannerViewState extends State<MobileScannerView> {

  final StreamController<BarcodeCapture> streamControllerMobileScanner = StreamController<BarcodeCapture>.broadcast();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Mobile Scanner')),
      body: StreamBuilder<BarcodeCapture?>(
        stream: streamControllerMobileScanner.stream,
        builder: (context, snapshot){
          return Column(
            children: [
              Expanded(
                flex: 6,
                child: MobileScanner(
                  // fit: BoxFit.contain,
                  controller: MobileScannerController(
                      detectionSpeed: DetectionSpeed.normal,
                      facing: CameraFacing.back,
                      cameraResolution: Size.zero
                    // torchEnabled: true,
                  ),
                  onDetect: (capture) {
                    streamControllerMobileScanner.sink.add(capture);
                  },
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: (snapshot.data != null)
                        ? Text(
                        'Barcode Type: ${describeEnum(snapshot.data?.barcodes.first.type ?? '')}\nData: ${snapshot.data?.barcodes.first.displayValue ?? ''}')
                        : const Text('Scan a code'),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
