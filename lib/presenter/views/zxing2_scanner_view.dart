import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:zxing2/qrcode.dart';

class Zxing2Scanner extends StatefulWidget {
  const Zxing2Scanner({super.key});

  @override
  State<Zxing2Scanner> createState() => _Zxing2ScannerState();
}

class _Zxing2ScannerState extends State<Zxing2Scanner> {
  final StreamController<Result> dataResultObs = StreamController<Result>.broadcast();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Zxing2 Scanner')),
      body: StreamBuilder<Result>(
        stream: dataResultObs.stream,
        builder: (context, snapshot){
          return SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 7,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.black87
                          ),
                          onPressed: () async {
                            final ImagePicker picker = ImagePicker();
                            final XFile? image = await picker.pickImage(source: ImageSource.gallery, imageQuality: 50, maxHeight: 150, maxWidth: 150);

                            _decodeImageQrCode(image?.path ?? '');
                          },
                          child: const Text('Image Gallery', style: TextStyle(color: Colors.white, fontSize: 32),)
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: (snapshot.data != null)
                          ? Text(
                          'Barcode Type: ${describeEnum(snapshot.data?.format ?? '')}\nData: ${snapshot.data?.text ?? ''}')
                          : const Text('Scan a code'),
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Future<void> _decodeImageQrCode(String pathImage) async {
    late final image;

    if(pathImage.contains('.jpg')){
      image = img.decodeImage(File(pathImage).readAsBytesSync())!;
    }else{
      image = img.decodePng(File(pathImage).readAsBytesSync())!;
    }


    LuminanceSource source = RGBLuminanceSource(
        image.width,
        image.height,
        image
            .convert(numChannels: 4)
            .getBytes(order: img.ChannelOrder.abgr)
            .buffer
            .asInt32List());
    var bitmap = BinaryBitmap(GlobalHistogramBinarizer(source));

    var reader = QRCodeReader();
    var result = reader.decode(bitmap);

    dataResultObs.sink.add(result);

    await _encodeImageQrCode(pathImage);
  }

  Future<void> _encodeImageQrCode(String pathImage) async {
    var qrcode = Encoder.encode('ABCDEF', ErrorCorrectionLevel.h);
    var matrix = qrcode.matrix!;
    var scale = 4;

    var image = img.Image(
        width: matrix.width * scale,
        height: matrix.height * scale,
        numChannels: 4);
    for (var x = 0; x < matrix.width; x++) {
      for (var y = 0; y < matrix.height; y++) {
        if (matrix.get(x, y) == 1) {
          img.fillRect(image,
              x1: x * scale,
              y1: y * scale,
              x2: x * scale + scale,
              y2: y * scale + scale,
              color: img.ColorRgba8(0, 0, 0, 0xFF));
        }
      }
    }
    var pngBytes = img.encodePng(image);
    final data = await File(pathImage).writeAsBytes(pngBytes);
    print('Image Encode ====> $data');
  }
}
